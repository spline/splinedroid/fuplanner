package de.sebse.fuplanner.fragments.canteen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.services.canteen.types.Canteens;
import de.sebse.fuplanner.tools.Preferences;
import de.sebse.fuplanner.tools.logging.Logger;
import de.sebse.fuplanner.tools.ui.CanteensViewHolder;

class CanteensAddAdapter extends RecyclerView.Adapter<CanteensViewHolder> {
    private int[] mElements;
    private Canteens mCanteens = null;
    @Nullable
    private final CanteenAddedInterface mCanteenAddedInterface;
    private Canteens mValues = null;
    private String mFilter = "";
    private final Context mContext;
    Logger log = new Logger(this);

    public CanteensAddAdapter(Context context, @Nullable CanteenAddedInterface canteenAddedInterface) {
        mCanteenAddedInterface = canteenAddedInterface;
        mContext = context;
        mElements = Preferences.getArrayInt(mContext, R.string.pref_canteen_selection);
    }

    public void setFilterString(String filter) {
        mFilter = filter;
        this.update();
    }

    public void setCanteens(Canteens canteens) {
        mCanteens = canteens;
        this.update();
    }

    public void update() {
        if (mCanteens == null || mFilter == null) {
            mValues = null;
        } else {
            mValues = new Canteens();
            for (Canteen canteen : mCanteens) {
                boolean found = false;
                for (int element : mElements) {
                    if (element == canteen.getId()) {
                        found = true;
                        break;
                    }
                }
                if (!found && canteen.contains(mFilter)) {
                    mValues.addCanteen(canteen);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CanteensViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_canteens_items, parent, false);
        return new CanteensViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CanteensViewHolder holder, int position) {
        if (mValues == null)
            return;
        Canteen canteen = mValues.get(position);
        holder.mTitle.setText(canteen.getName());
        holder.mSubLeft.setText(canteen.getAddress());
        holder.mSubRight.setText(canteen.getCity());
        holder.mActionIcon.setVisibility(View.VISIBLE);
        holder.mActionIcon.setImageDrawable(ResourcesCompat.getDrawable(holder.mView.getResources(), R.drawable.ic_add_circle_outline, null));
        holder.mActionIcon.setOnClickListener(v -> {
            mElements  = Arrays.copyOf(mElements, mElements.length + 1);
            mElements[mElements.length - 1] = canteen.getId();

            Preferences.setArrayInt(mContext, R.string.pref_canteen_selection, mElements);
            if (mCanteenAddedInterface != null)
                mCanteenAddedInterface.onCanteenAdded(canteen.getId());
            update();
        });
    }

    @Override
    public int getItemCount() {
        if (mValues != null) {
            return mValues.size();
        }
        return 0;
    }

    interface CanteenAddedInterface {
        void onCanteenAdded(int canteenId);
    }
}
