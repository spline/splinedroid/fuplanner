package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class StringViewHolder extends CustomViewHolder {
    public final TextView mString;

    public StringViewHolder(View view) {
        super(view);
        mString = view.findViewById(R.id.string);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mString.getText() + "'";
    }
}
