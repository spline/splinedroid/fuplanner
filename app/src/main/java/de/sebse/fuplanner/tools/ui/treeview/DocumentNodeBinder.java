package de.sebse.fuplanner.tools.ui.treeview;

import android.view.View;
import android.widget.TextView;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;

import androidx.core.content.ContextCompat;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Resource;
import de.sebse.fuplanner.services.kvv.ui.Download;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.ui.cardview.ExpandableCardView;

/**
 * Created by tlh on 2016/10/1 :)
 */

public class DocumentNodeBinder extends TreeViewBinder<DocumentNodeBinder.ViewHolder> {

    private final Download.OnDownloadRequestInterface requestInterface;

    public DocumentNodeBinder(Download.OnDownloadRequestInterface requestInterface) {
        super();
        this.requestInterface = requestInterface;
    }

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, TreeNode node) {
        Resource.Document fileNode = (Resource.Document) node.getContent();
        holder.title.setText(fileNode.getTitle());
        holder.sub_title.setText(UtilsDate.getModifiedDateTime(holder.itemView.getContext(), fileNode.getModifiedDate()));
        holder.notes.setText(fileNode.getBody());

        holder.mTagGroup.removeAll();
        for (int i = 0, notesSize = fileNode.size(); i < notesSize; i++) {
            String name = fileNode.getUrl(i).name;
            Tag tag = new Tag(name.substring(0, Math.min(40, name.length())));
            tag.id = i;
            tag.layoutColor = ContextCompat.getColor(holder.itemView.getContext(), R.color.colorFUBlue);
            holder.mTagGroup.addTag(tag);
        }
        holder.mTagGroup.setOnTagClickListener((tag, i) -> {
            String s = fileNode.getUrl(i).url;
            if (s != null) {
                String name = fileNode.getUrl(i).name;
                this.requestInterface.request(name, s);
            }
        });
        holder.itemView.invalidate();
    }

    @Override
    public int getLayoutId() {
        return R.layout.list_announcement_items;
    }

    public class ViewHolder extends TreeViewBinder.ViewHolder {
        final TextView title;
        final TextView sub_title;
        final TextView notes;
        final ExpandableCardView card_view;
        final TagView mTagGroup;

        ViewHolder(View rootView) {
            super(rootView);
            this.card_view = rootView.findViewById(R.id.card_view);
            this.title = this.card_view.getOuterView().findViewById(R.id.title);
            this.sub_title = this.card_view.getOuterView().findViewById(R.id.sub_title);
            this.notes = this.card_view.getInnerView().findViewById(R.id.notes);
            this.mTagGroup = this.card_view.getInnerView().findViewById(R.id.tag_group);
        }

    }
}
