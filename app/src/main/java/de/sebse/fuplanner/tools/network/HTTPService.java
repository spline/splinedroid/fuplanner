package de.sebse.fuplanner.tools.network;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import de.sebse.fuplanner.tools.EventListener;
import de.sebse.fuplanner.tools.NewAsyncQueue;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * Created by sebastian on 24.10.17.
 */

public class HTTPService {
    private final Context mContext;
    private final NewAsyncQueue mQueue = new NewAsyncQueue();
    protected final Logger log = new Logger(this);

    private int mRequestCount = 0;
    private final HashMap<String, EventListener.EventFunction<VolleyError>> errorListeners = new HashMap<>();
    private final HashMap<String, EventListener.EventFunction<Result>> successListeners = new HashMap<>();
    private CountDownTimer mDisconnectTimer = null;
    private HTTPNetwork mService;
    private boolean mBound = false;
    private boolean mWaitForBound = false;
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            HTTPNetwork.LocalBinder binder = (HTTPNetwork.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            for (String listenerKey: errorListeners.keySet()) {
                mService.addErrorListener(listenerKey, errorListeners.get(listenerKey));
            }
            for (String listenerKey: successListeners.keySet()) {
                mService.addSuccessListener(listenerKey, successListeners.get(listenerKey));
            }
            if (mWaitForBound) {
                mWaitForBound = false;
                mQueue.next();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService = null;
        }
    };

    public HTTPService(Context context) {
        this.mContext = context;
    }

    public void addErrorListener(String id, EventListener.EventFunction<VolleyError> listener) {
        errorListeners.put(id, listener);
        connect();
        mQueue.add(() -> {
            mService.addErrorListener(id, listener);
            mQueue.next();
            disconnect();
        });
    }

    public void removeErrorListener(String id) {
        errorListeners.remove(id);
        connect();
        mQueue.add(() -> {
            mService.removeErrorListener(id);
            mQueue.next();
            disconnect();
        });
    }

    public void addSuccessListener(String id, EventListener.EventFunction<Result> listener) {
        successListeners.put(id, listener);
        connect();
        mQueue.add(() -> {
            mService.addSuccessListener(id, listener);
            mQueue.next();
            disconnect();
        });
    }

    public void removeSuccessListener(String id) {
        successListeners.remove(id);
        connect();
        mQueue.add(() -> {
            mService.removeSuccessListener(id);
            mQueue.next();
            disconnect();
        });
    }

    protected void head(String url, @Nullable final HashMap<String, String> cookies, Response.Listener<Result> response, Response.ErrorListener error) {
        connect();
        mQueue.add(() -> {
            mService.head(url, cookies, response1 -> {
                response.onResponse(response1);
                disconnect();
            }, error1 -> {
                error.onErrorResponse(error1);
                disconnect();
            });
            mQueue.next();
        });
    }

    protected void get(String url, @Nullable final HashMap<String, String> cookies, Response.Listener<Result> response, Response.ErrorListener error) {
        connect();
        mQueue.add(() -> {
            mService.get(url, cookies, response1 -> {
                response.onResponse(response1);
                disconnect();
            }, error1 -> {
                error.onErrorResponse(error1);
                disconnect();
            });
            mQueue.next();
        });
    }

    protected void post(String url, @Nullable final HashMap<String, String> cookies, @Nullable final HashMap<String, String> body, Response.Listener<Result> response, Response.ErrorListener error) {
        connect();
        mQueue.add(() -> {
            mService.post(url, cookies, body, response1 -> {
                response.onResponse(response1);
                disconnect();
            }, error1 -> {
                error.onErrorResponse(error1);
                disconnect();
            });
            mQueue.next();
        });
    }

    private void connect() {
        if (!mBound) {
            Intent intent = new Intent(getContext(), HTTPNetwork.class);
            getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            if (!mWaitForBound) mQueue.add(() -> {});
            mWaitForBound = true;
        }
        if (mDisconnectTimer != null) {
            mDisconnectTimer.cancel();
            mDisconnectTimer = null;
        }
        mRequestCount++;
    }

    private void disconnect() {
        mRequestCount--;
        if (mDisconnectTimer == null) {
            if (mBound && mRequestCount == 0) {
                mBound = false;
                mService = null;
                getContext().unbindService(mConnection);
            }
        }
    }


    protected String getCookie(String cookies, String name) throws NoSuchFieldException {
        Pattern pattern = Pattern.compile(name+"=([^;]+);");
        Matcher matcher = pattern.matcher(cookies);
        if (!matcher.find()) {
            log.e("GETcookie failed", name);
            log.e("GETcookie failed", cookies);
            throw new NoSuchFieldException();
        }
        return matcher.group(1);
    }

    protected HashMap<String, String> getCookie(String cookies, String[] names) throws NoSuchFieldException {
        HashMap<String, String> result = new HashMap<>();
        for (String name: names) {
            result.put(name,this.getCookie(cookies, name));
        }
        return result;
    }

    protected Context getContext() {
        return mContext;
    }
}
