package de.sebse.fuplanner.tools.ui;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class CustomViewHolder extends RecyclerView.ViewHolder {
    public final View mView;

    public CustomViewHolder(View view) {
        super(view);
        mView = view;
    }
}
