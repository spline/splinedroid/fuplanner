package de.sebse.fuplanner.tools.types;

import androidx.annotation.NonNull;

public class News {
    public static final int CATEGORY_TRICKS = 0;
    public static final int CATEGORY_UPDATE = 1;

    private final String title;
    private final int category;
    private final long date;
    private final String text;

    public News(String title, int category, long date, String text) {
        this.title = title;
        this.category = category;
        this.date = date;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public int getCategory() {
        return category;
    }

    public long getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    @NonNull
    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", category=" + category +
                ", date=" + date +
                ", text='" + text + '\'' +
                '}';
    }


}
