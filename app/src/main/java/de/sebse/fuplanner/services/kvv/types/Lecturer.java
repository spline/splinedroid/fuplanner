package de.sebse.fuplanner.services.kvv.types;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;

public class Lecturer implements Serializable {
    private final String firstName;
    private final String surname;
    private final String mail;
    private final boolean isResponsible;

    public Lecturer(String firstName, String surname, String mail, boolean isResponsible) {
        this.firstName = firstName;
        this.surname = surname;
        this.mail = mail;
        this.isResponsible = isResponsible;
    }

    public Lecturer(String parsableString) throws NoSuchFieldException {
        Pattern pattern = Pattern.compile("([^|]*)\\|([^|]*)\\|([^|]*)\\|\\|([^|]*)", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(parsableString);
        if (!matcher.find()) {
            throw new NoSuchFieldException();
        }
        this.firstName = matcher.group(1);
        this.surname = matcher.group(2);
        this.mail = matcher.group(3);
        this.isResponsible = matcher.group(4).equals("true");
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getMail() {
        return mail;
    }

    public boolean isResponsible() {
        return isResponsible;
    }

    public String getName() {
        return getFirstName() + " " + getSurname();
    }

    public String getNameShort() {
        return getFirstName().charAt(0) + ". " + getSurname();
    }

    @NonNull
    @Override
    public String toString() {
        return "First name: "+ getFirstName()+
                "\nSurname: "+getSurname()+
                "\nMail: "+getMail();
    }
}
