package de.sebse.fuplanner.services.kvv;

import android.content.Context;

import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.network.HTTPService;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

public abstract class Part<T> extends HTTPService {
    static final int RETRY_COUNT = 1;
    final Login mLogin;
    final ModulesList mList;

    Part(Login login, ModulesList list, Context context) {
        super(context);
        this.mLogin = login;
        this.mList = list;
    }

    public void recv(final String moduleID, final NetworkCallback<T> callback, final NetworkErrorCallback errorCallback) {
        recv(moduleID, callback, errorCallback, false);
    }

    public void recv(final String moduleID, final NetworkCallback<T> callback, final NetworkErrorCallback errorCallback, final boolean forceRefresh) {
        mList.find(moduleID, success -> recv(success, callback, errorCallback, forceRefresh), errorCallback);
    }

    public void recv(final Modules.Module module, final NetworkCallback<T> callback, final NetworkErrorCallback errorCallback, final boolean forceRefresh) {
        recv(module, callback, errorCallback, forceRefresh, RETRY_COUNT);
    }

    abstract protected void recv(final Modules.Module module, final NetworkCallback<T> callback, final NetworkErrorCallback errorCallback, final boolean forceRefresh, final int retries);
}
