package de.sebse.fuplanner.services.canteen.types;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Iterator;

public class Day implements Serializable, Iterable<Meal> {
    private final int canteenId;
    private final Calendar calendar;
    private final boolean isClosed;
    private SortedListMeal list = new SortedListMeal();

    Day(Canteen canteen, Calendar calendar, boolean isClosed) {
        this.canteenId = canteen.getId();
        this.calendar = calendar;
        this.isClosed = isClosed;
    }

    public Meal getMeal(Integer id) {
        return this.list.getById(id);
    }

    public void addMeal(int id, String name, String category, double priceStdnt, double priceEmply, double priceOther, String[] notes) {
        Meal meal = new Meal(id, name, category, priceStdnt, priceEmply, priceOther, notes);
        if (this.list.contains(meal)) return;
        this.list.add(meal);
    }

    public int size() {
        return this.list.size();
    }

    public Meal get(int index) {
        return this.list.get(index);
    }

    public void update(Day day) {
        this.list = day.list;
    }

    @NonNull
    @Override
    public Iterator<Meal> iterator() {
        return this.list.iterator();
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public int getCanteenId() {
        return canteenId;
    }

    @NonNull
    @Override
    public String toString() {
        return Canteen.calendarToKey(getCalendar())+"\n"+this.list+"\n";
    }
}
