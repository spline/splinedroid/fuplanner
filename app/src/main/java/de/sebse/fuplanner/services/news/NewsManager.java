package de.sebse.fuplanner.services.news;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;
import de.sebse.fuplanner.tools.types.News;
import de.sebse.fuplanner.tools.types.NewsList;

public class NewsManager {
    private NewsList mNewsList;
    private final Context mContext;

    public NewsManager(Context context) {
        this.mContext = context;
    }

    public void recv(final NetworkCallback<NewsList> callback, final NetworkErrorCallback errorCallback) {
        recv(callback, errorCallback, false);
    }

    public void recv(final NetworkCallback<NewsList> callback, final NetworkErrorCallback errorCallback, final boolean forceRefresh) {
        if (!forceRefresh && mNewsList != null) {
            callback.onResponse(mNewsList);
            return;
        }
        String fromAsset = loadJSONFromAsset();
        if (fromAsset == null)
            return;
        String language = Locale.getDefault().getLanguage();
        JSONArray news;
        NewsList dates = new NewsList();
        try {
            JSONObject json = new JSONObject(fromAsset);
            news = json.getJSONArray("news");
        } catch (JSONException e) {
            e.printStackTrace();
            errorCallback.onError(new NetworkError(300100, 500, "Parsing news list failed!"));
            return;
        }

        for (int i = news.length() - 1; i >= 0; i--) {
            try {
                String title;
                if (news.getJSONObject(i).has("title_" + language)) {
                    title = news.getJSONObject(i).getString("title_" + language);
                } else {
                    title = news.getJSONObject(i).getString("title");
                }
                String text;
                if (news.getJSONObject(i).has("text_" + language)) {
                    text = news.getJSONObject(i).getString("text_" + language);
                } else {
                    text = news.getJSONObject(i).getString("text");
                }

                String categoryString = news.getJSONObject(i).getString("category");
                int category;
                if (categoryString.equals("CATEGORY_TRICKS"))
                    category = News.CATEGORY_TRICKS;
                else
                    category = News.CATEGORY_UPDATE;
                String dateString = news.getJSONObject(i).getString("date");
                long date = UtilsDate.stringToMillis(dateString, "dd.MM.yyyy");
                News event = new News(title, category, date, text);
                dates.add(event);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mNewsList = dates;
        callback.onResponse(mNewsList);
    }

    private String loadJSONFromAsset() {
        try {
            InputStream is = mContext.getResources().openRawResource(R.raw.news);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
