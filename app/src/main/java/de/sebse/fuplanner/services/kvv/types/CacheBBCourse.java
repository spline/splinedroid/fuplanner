package de.sebse.fuplanner.services.kvv.types;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

public class CacheBBCourse implements Serializable {
    private transient static final long RESAVE_TIMER = 1000L * 60 * 60 * 24 * 30;
    private static final String FILE_NAME = "BBCourseStorageSaving";
    private static final String FILE_NAME_TIMESTAMP = "BBCourseStorageSavingTimestamp";
    private transient long mLastTimestamp = 0;

    private final HashSet<String> mKVVCourseList = new HashSet<>();
    private long mKVVCourseListRefresh = 0;
    private final HashMap<String, Modules.Module> mBBCourseList = new HashMap<>();
    private final HashMap<String, Long> mBBCourseListRefresh = new HashMap<>();

    public static CacheBBCourse load(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis;
        try {
            fis = context.openFileInput(FILE_NAME);
        } catch (FileNotFoundException e) {
            return null;
        }
        ObjectInputStream is = new ObjectInputStream(fis);
        Object readObject = is.readObject();
        if (!(readObject instanceof CacheBBCourse))
            return null;
        CacheBBCourse storage = (CacheBBCourse) readObject;
        is.close();
        fis.close();

        fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        is = new ObjectInputStream(fis);
        storage.mLastTimestamp = is.readLong();
        is.close();
        fis.close();

        return storage;
    }

    public boolean isNewerVersionInStorage(Context context) throws IOException {
        FileInputStream fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        ObjectInputStream is = new ObjectInputStream(fis);
        boolean result = this.mLastTimestamp < is.readLong();
        is.close();
        fis.close();
        return result;
    }

    public void save(Context context) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(this);
        os.close();
        fos.close();

        fos = context.openFileOutput(FILE_NAME_TIMESTAMP, Context.MODE_PRIVATE);
        os = new ObjectOutputStream(fos);
        this.mLastTimestamp = System.currentTimeMillis();
        os.writeLong(this.mLastTimestamp);
        os.close();
        fos.close();
    }

    public void addKVVCourseID(String courseID) {
        mKVVCourseList.add(courseID);
        mKVVCourseListRefresh = System.currentTimeMillis();
    }

    public boolean hasKVVCourseID(String courseID) {
        if (mKVVCourseListRefresh + RESAVE_TIMER < System.currentTimeMillis())
            return false;
        return mKVVCourseList.contains(courseID);
    }

    public void setBBCourse(String courseID, Modules.Module module) {
        mBBCourseList.put(courseID, module);
        mBBCourseListRefresh.put(courseID, System.currentTimeMillis());
    }

    public Modules.Module getBBCourse(String courseID) {
        if (!mBBCourseListRefresh.containsKey(courseID))
            return null;
        //noinspection ConstantConditions
        if (mBBCourseListRefresh.get(courseID) + RESAVE_TIMER < System.currentTimeMillis())
            return null;
        return mBBCourseList.get(courseID);
    }
}
