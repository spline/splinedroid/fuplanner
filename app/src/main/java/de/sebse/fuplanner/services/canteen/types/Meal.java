package de.sebse.fuplanner.services.canteen.types;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;

public class Meal implements Serializable {
    private static final int LIGHT_NONE = 0;
    private static final int LIGHT_GREEN = 1;
    private static final int LIGHT_YELLOW = 2;
    private static final int LIGHT_RED = 3;
    private static final int VEGAN_NONE = 0;
    public static final int VEGAN_VEGETARIAN = 1;
    public static final int VEGAN_VEGAN = 2;
    private static final int CERT_NONE = 0b0000;
    public static final int CERT_BIO =  0b0001;
    public static final int CERT_MSC =  0b0010;

    private final int id;
    private final String name;
    private final String category;
    private final double priceStdnt;
    private final double priceEmply;
    private final double priceOther;
    private final List<String> notes;
    private final int light;
    private final int vegan;
    private final int certificates;

    Meal(int id, String name, String category, double priceStdnt, double priceEmply, double priceOther, String[] notes) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.priceStdnt = priceStdnt;
        this.priceEmply = priceEmply;
        this.priceOther = priceOther;
        int light = LIGHT_NONE;
        int vegan = VEGAN_NONE;
        int certificates = CERT_NONE;
        List<String> filteredNotes = new ArrayList<>();
        for (String note : notes) {
            switch (note.toLowerCase()) {
                case "vegetarisch":
                    vegan = VEGAN_VEGETARIAN;
                    break;
                case "vegan":
                    vegan = VEGAN_VEGAN;
                    break;
                case "bio":
                    certificates += CERT_BIO;
                    break;
                case "msc":
                    certificates += CERT_MSC;
                    break;
                case "grün (ampel)":
                    light = LIGHT_GREEN;
                    break;
                case "gelb (ampel)":
                    light = LIGHT_YELLOW;
                    break;
                case "rot (ampel)":
                    light = LIGHT_RED;
                    break;
                default:
                    filteredNotes.add(note);
            }
        }
        this.light = light;
        this.vegan = vegan;
        this.certificates = certificates;
        this.notes = Collections.unmodifiableList(filteredNotes);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public double getPriceStdnt() {
        return priceStdnt;
    }

    public double getPriceEmply() {
        return priceEmply;
    }

    public double getPriceOther() {
        return priceOther;
    }

    public List<String> getNotes() {
        return notes;
    }

    public int getLight() {
        return light;
    }

    public int getVegan() {
        return vegan;
    }

    public int getCertificates() {
        return certificates;
    }

    @NonNull
    @Override
    public String toString() {
        return name + " (" + category + ")";
    }
}
