package de.sebse.fuplanner.services.kvv.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.JsonReader;
import android.util.JsonToken;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Resource;
import de.sebse.fuplanner.tools.Regex;
import de.sebse.fuplanner.tools.RequestPermissionsResultListener;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.logging.Logger;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class Download {

    private final ContextInterface contextInterface;
    private final ActivityInterface activityInterface;
    private final Logger log = new Logger(this);
    private RequestedDownload requestedDownload;
    private Map<String, String> knownTypes;

    public Download(ContextInterface contextInterface, ActivityInterface activityInterface) {
        this.contextInterface = contextInterface;
        this.activityInterface = activityInterface;
    }

    public void openDownloadDialog(String title, String url, String folderName) {
        openDownloadDialog(new Resource.File("", title, 0, url, true, "", ""), folderName);
    }

    public void openDownloadDialog(Resource.File file, String folderName) {
        Context context = contextInterface.get();
        if (context == null)
            return;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        StringBuilder sb = new StringBuilder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            sb.append(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS));
        } else {
            sb.append(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
        }
        sb.append('/').append(folderName).append('/').append(file.getTitle());
        File f = new File(sb.toString());

        Resources resources = context.getResources();
        String message = "";
        if (file.getAuthor() != null && !file.getAuthor().isEmpty())
            message += resources.getString(R.string.creator_name, file.getAuthor());
        if (file.getModifiedDate() != 0) {
            if (!message.isEmpty())
                message += "\n";
            message += resources.getString(R.string.last_modified_on, UtilsDate.getModifiedDateTime(context, file.getModifiedDate()));
        }

        alertDialogBuilder
                .setTitle(file.getTitle())
                .setMessage(message)
                .setCancelable(true)
                .setNeutralButton(R.string.close, (dialog, id) -> dialog.cancel());
        // if already downloaded, show open button
        if (f.exists()) {
            alertDialogBuilder
                    .setPositiveButton(R.string.download_again, (dialog, id) -> download(file, folderName, true))
                    .setNegativeButton(R.string.openFile, (dialog, id) -> download(file, folderName, false));
        } else {
            alertDialogBuilder
                    .setPositiveButton(R.string.download, (dialog, id) -> download(file, folderName, true));
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void download(Resource.File file, String folderName, boolean downloadNew) {
        MainActivity activity = activityInterface.get();
        if (activity == null) {
            showDownloadError();
            return;
        }
        if (checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // Access granted
            downloadOrOpen(file, folderName, downloadNew);
        } else {
            this.requestedDownload = new RequestedDownload(file, folderName, downloadNew);
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }
    }

    private void downloadOrOpen(Resource.File file, String folderName, boolean downloadNew) {
        if (!isExternalStorageWritable()) {
            return;
        }

        MainActivity activity = activityInterface.get();
        if (activity == null) {
            showDownloadError();
            return;
        }

        activity.getKVV(kvv -> {
            kvv.modules().resources().file(file.getTitle(), file.getUrl(), folderName, success -> {
                Context context = contextInterface.get();
                if (success.isEmpty() || context == null) {
                    showDownloadError();
                } else {
                    if (Regex.has("^http", success)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(success));
                        context.startActivity(intent);
                    } else {
                        fileOpen(new File(success));
                    }
                }
            }, log::e, downloadNew);
        });
    }

    private void showDownloadError() {
        Context context = contextInterface.get();
        if (context == null)
            return;

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.ErrorFileDownload)
                .setMessage(R.string.ErrorFileDownloadText)
                .setCancelable(true)
                .setNeutralButton(R.string.close, (dialog, id) -> dialog.cancel())
                .create();
        alertDialog.show();
    }

    public RequestPermissionsResultListener getRequestPermissionsResultListener() {
        return (requestCode, permissions, grantResults) -> {
            if (requestedDownload == null) {
                log.e("No request");
                return;
            }
            if (activityInterface.get() == null) {
                showDownloadError();
                return;
            }
            int pos = Arrays.asList(permissions).indexOf("android.permission.WRITE_EXTERNAL_STORAGE");
            if (pos != -1) {
                if (grantResults[pos] != -1) {
                    downloadOrOpen(requestedDownload.file, requestedDownload.folderName, requestedDownload.downloadNew);
                } else {
                    showDownloadError();
                }
                requestedDownload = null;
            }
        };
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        log.e("File system: Writing not possible");
        return false;
    }

    private void fileOpen(File url) {
        log.d("Attempting to open file", url.toString());

        // resolve the context
        Context context = contextInterface.get();
        if (context == null) {
            log.w("Cancelled opening file, context not found");
            return;
        }

        // generate a URI and try to identify the file type
        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".my.provider", url);
        String MIMEType = context.getContentResolver().getType(uri);
        log.d("Detected MIME type", MIMEType);

        // create a new intent to open the file
        Intent intent;
        intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        if (MIMEType.equals("application/octet-stream")) {
            log.d("MIME type could not be detected automatically, trying to match file extension");

            // we just found the generic "catch all" mime type, check against our own list
            Map<String, String> types = getKnownTypes(context);
            String path = uri.getPath();
            String ext = path.substring(path.lastIndexOf('.') + 1);

            log.d("Extracted extension", ext);

            MIMEType = types.get(ext.toLowerCase());
            if (MIMEType == null) {
                MIMEType = "*/*";
            }
        }

        // set the MIME type in the intent so Android knows which apps to display
        log.d("Setting MIME type", MIMEType);
        intent.setDataAndType(uri, MIMEType);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            log.e("No matching activity found for MIME type", MIMEType);

            if (MIMEType.equals("*/*")) {
                // this was the "any activity" MIME type already, give up
                return;
            }

            // retry with the "any activity" MIME type
            try {
                intent.setDataAndType(uri, "*/*");
                context.startActivity(intent);
            } catch (ActivityNotFoundException f) {
                log.e("No matching activity found for MIME type */*, giving up");
                toast(context, R.string.error_could_not_open_file, uri.getPath());
            }
        }
    }

    private Map<String, String> getKnownTypes(Context context) {
        if (knownTypes == null) {
            knownTypes = loadKnownTypes(context);
        }

        return knownTypes;
    }

    private Map<String, String> loadKnownTypes(Context context) {
        Map<String, String> knownTypes = new HashMap<>();
        String key;
        String value;

        InputStream is = context.getResources().openRawResource(R.raw.known_types);
        try (JsonReader jr = new JsonReader(new InputStreamReader(is))) {
            jr.beginObject();

            while (jr.peek() == JsonToken.NAME) {
                key = jr.nextName().toLowerCase();
                value = jr.nextString();
                knownTypes.put(key, value);
            }

        } catch (IOException e) {
            log.e("Could not decode the JSON resource", e);
        }

        return knownTypes;
    }

    @FunctionalInterface
    public interface ContextInterface {
        @Nullable
        Context get();
    }

    @FunctionalInterface
    public interface ActivityInterface {
        @Nullable
        MainActivity get();
    }

    public interface OnDownloadRequestInterface {
        void request(String title, String url);
    }

    private class RequestedDownload {
        final Resource.File file;
        final String folderName;
        final boolean downloadNew;

        RequestedDownload(Resource.File file, String folderName, boolean downloadNew) {
            this.file = file;
            this.folderName = folderName;
            this.downloadNew = downloadNew;
        }
    }

    private void toast(Context context, int resID, Object... params) {
        Toast.makeText(
                context,
                context.getResources().getString(resID, params),
                Toast.LENGTH_LONG
        ).show();
    }
}
