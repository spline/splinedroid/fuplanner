package de.sebse.fuplanner.services.kvv.sync;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SyncResult;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.fragments.moddetails.ModulePart;
import de.sebse.fuplanner.services.kvv.KVV;
import de.sebse.fuplanner.services.kvv.types.Announcement;
import de.sebse.fuplanner.services.kvv.types.Assignment;
import de.sebse.fuplanner.services.kvv.types.AssignmentList;
import de.sebse.fuplanner.services.kvv.types.Event;
import de.sebse.fuplanner.services.kvv.types.EventList;
import de.sebse.fuplanner.services.kvv.types.Grade;
import de.sebse.fuplanner.services.kvv.types.Gradebook;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.services.kvv.types.Resource;
import de.sebse.fuplanner.tools.CustomNotificationManager;
import de.sebse.fuplanner.tools.NewAsyncQueue;
import de.sebse.fuplanner.tools.Preferences;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.logging.Logger;

import static android.provider.CalendarContract.CALLER_IS_SYNCADAPTER;
import static de.sebse.fuplanner.MainActivity.FRAGMENT_MODULES_DETAILS;

public class KVVSyncAdapter extends AbstractThreadedSyncAdapter {
    private KVV mKVV;
    private final Logger log = new Logger(this);
    private final NewAsyncQueue mQueue = new NewAsyncQueue();


    private boolean mBound = false;
    private boolean mWaitForBound = false;
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            KVV.LocalBinder binder = (KVV.LocalBinder) service;
            mKVV = binder.getService();
            mBound = true;
            if (mWaitForBound) {
                mWaitForBound = false;
                mQueue.next();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mKVV = null;
        }
    };


    /**
     * Set up the sync adapter
     */
    KVVSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        init(context);
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public KVVSyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        init(context);
    }

    private void init(Context context) {
        // Bind to LocalService
    }

    /*
     * Specify the code you want to run in the sync adapter. The entire
     * sync adapter runs in a background thread, so you don't have to set
     * up your own background processing.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        if (!mBound && !mWaitForBound) {
            Intent intent = new Intent(getContext(), KVV.class);
            getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mWaitForBound = true;
            mQueue.add(() -> {});
        }

        mQueue.add(() -> {
            mKVV.account().restoreOnlineLogin(bool -> {
                mQueue.next();
            });
        });

        mQueue.add(() -> {
            if (!mKVV.account().isLoggedIn()) {
                log.w("Not logged in!");
                mQueue.next();
                return;
            }

            mKVV.modules().list().reloadIfOutdated();
            mKVV.modules().list().recv(success -> {
                final int[] latch = {0};
                Iterator<Modules.Module> iterator = success.latestSemesterIterator();
                while (iterator.hasNext()) {
                    latch[0] += 1;
                    iterator.next();
                }
                boolean doAddToCalendar = createCalendar(account);
                log.d("sync calendar", doAddToCalendar);
                iterator = success.latestSemesterIterator();
                while (iterator.hasNext()) {
                    Modules.Module module = iterator.next();
                    final ArrayList<Announcement> announcements = module.announcements;
                    final AssignmentList assignments = module.assignments;
                    final EventList events = module.events;
                    final Gradebook gradebook = module.gradebook;
                    final ArrayList<Resource> resources = module.resources;
                    mKVV.modules().details().recv(module, success1 -> {
                        if (success1.second) {
                            sendNotifications(announcements, module.announcements, module.title, Announcement::getTitle, Announcement::getId,
                                    module.getID(), ModulePart.ANNOUNCEMENT,
                                    R.string.announcement_updated, R.string.announcement_added, R.string.announcement_removed);
                            sendNotifications(assignments, module.assignments, module.title, Assignment::getTitle, Assignment::getId,
                                    module.getID(), ModulePart.ASSIGNMENT,
                                    R.string.assignment_updated, R.string.assignment_added, R.string.assignment_removed);
                            //ArrayList<Event> differencesAdd = new ArrayList<>();
                            //ArrayList<Pair<Event, Event>> differencesUpd = new ArrayList<>();
                            //ArrayList<Event> differencesDel = new ArrayList<>();
                            sendNotifications(events, module.events, module.title, evt -> evt.getTitle()+" - "+UtilsDate.getModifiedDate(evt.getStartDate()), event -> event.getStartDate() +event.getType()+event.getTitle(),
                                    module.getID(), ModulePart.EVENT,
                                    R.string.event_updated, R.string.event_added, R.string.event_removed/*,
                                    differencesAdd, differencesUpd, differencesDel*/);
                            sendNotifications(gradebook, module.gradebook, module.title, Grade::getItemName, Grade::getItemName,
                                    module.getID(), ModulePart.GRADEBOOK,
                                    R.string.gradebook_updated, R.string.gradebook_added, R.string.gradebook_removed);
                            sendNotifications(resources, module.resources, module.title, Resource::getTitle, Resource::getUrl,
                                    module.getID(), ModulePart.RESOURCES,
                                    R.string.resource_updated, R.string.resource_added, R.string.resource_removed);
                            if (doAddToCalendar) {
                                addToCalendar(module.events, account);
                            }
                            if (--latch[0] == 0) {
                                forceSync();
                                mQueue.next();
                            }
                        }
                    }, error -> {
                        log.e(error);
                        if (--latch[0] == 0) {
                            forceSync();
                            mQueue.next();
                        }
                    }, true);
                }
            }, error -> {
                log.e(error);
                mQueue.next();
            }, true);
        });
        mQueue.add(() -> {
            if (mBound) {
                getContext().unbindService(mConnection);
            }
            mBound = false;
            mKVV = null;
            mQueue.next();
        });
    }

    static Uri asSyncAdapter(Uri uri, String account, String accountType) {
        return uri.buildUpon()
                .appendQueryParameter(CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(Calendars.ACCOUNT_NAME, account)
                .appendQueryParameter(Calendars.ACCOUNT_TYPE, accountType).build();
    }

    private static Uri createCalendarWithName(Context ctx, Account account, String calendarName) {
        String accountName = account.name;
        String accountType = account.type;
        Uri target = asSyncAdapter(Calendars.CONTENT_URI, accountName, accountType);

        ContentValues values = new ContentValues();
        values.put(Calendars._ID, calendarName.hashCode());
        values.put(Calendars.ACCOUNT_NAME, accountName);
        values.put(Calendars.ACCOUNT_TYPE, accountType);
        values.put(Calendars.NAME, calendarName);
        values.put(Calendars.CALENDAR_DISPLAY_NAME, calendarName);
        values.put(Calendars.CALENDAR_COLOR, 0x00FF00);
        values.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_READ);
        values.put(Calendars.OWNER_ACCOUNT, accountName);
        values.put(Calendars.VISIBLE, 1);
        values.put(Calendars.SYNC_EVENTS, 1);
        values.put(Calendars.CALENDAR_TIME_ZONE, "Europe/Berlin");
        values.put(Calendars.CAN_PARTIALLY_UPDATE, 1);
        values.put(Calendars.CAL_SYNC1, System.currentTimeMillis());

        return ctx.getContentResolver().insert(target, values);
    }

    private int createEvents(Context ctx, Account account, String calendarName, EventList events) {
        String accountName = account.name;
        String accountType = account.type;
        Uri target = asSyncAdapter(Events.CONTENT_URI, accountName, accountType);

        ContentValues[] contentValues = new ContentValues[events.size()];
        for (int i = 0; i < contentValues.length; i++) {
            Event event  = events.get(i);
            ContentValues values = new ContentValues();
            //values.put(Events._ID, event.hashCode());
            values.put(Events.TITLE, event.getTitle());
            values.put(Events.DTSTART, event.getStartDate());
            values.put(Events.DTEND, event.getEndDate());
            values.put(Events.CALENDAR_ID, calendarName.hashCode());
            contentValues[i] = values;
            //ctx.getContentResolver().insert(target, values);
        }
        log.d(contentValues.length);
        return ctx.getContentResolver().bulkInsert(target, contentValues);
    }

    private static Cursor getCalendars(Context ctx, Account account) {
        Uri target = asSyncAdapter(Calendars.CONTENT_URI, account.name, account.type);

        return ctx.getContentResolver().query(target, new String[]{Calendars.CALENDAR_DISPLAY_NAME, Calendars.ACCOUNT_NAME}, null, null, null);
    }

    private static Cursor getEvents(Context ctx, Account account) {
        Uri target = asSyncAdapter(Events.CONTENT_URI, account.name, account.type);

        return ctx.getContentResolver().query(target, new String[]{Events.TITLE, Events.DTSTART}, null, null, null);
    }

    private static int deleteCalendars(Context ctx, Account account) {
        String accountName = account.name;
        String accountType = account.type;
        Uri target = asSyncAdapter(Calendars.CONTENT_URI, account.name, account.type);

        ContentValues values = new ContentValues();
        values.put(Calendars.ACCOUNT_NAME, accountName);
        values.put(Calendars.ACCOUNT_TYPE, accountType);

        return ctx.getContentResolver().delete(target, null, null);
    }

    private void addToCalendar(EventList events, Account account) {
        if (events == null) {
            return;
        }
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            boolean integrationEnabled = Preferences.getBoolean(getContext(), R.string.pref_add_calendar);
            if (integrationEnabled) {
                createEvents(getContext(), account, getContext().getString(R.string.app_name), events);
            }
        }
    }

    private boolean createCalendar(Account account) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            boolean integrationEnabled = Preferences.getBoolean(getContext(), R.string.pref_add_calendar);
            if (integrationEnabled) {
                //log.w("No calendar found! Add calendar...");
                deleteCalendars(getContext(), account);
                createCalendarWithName(getContext(), account, getContext().getString(R.string.app_name));
                return true;
            } else {
                log.w("Calendar found and integration disabled! Delete calendar...");
                deleteCalendars(getContext(), account);
                return false;
            }
        } else {
            log.w("Permission calendar not granted!");
            return false;
        }
    }

    private void forceSync() {
        // Force a sync
        Bundle extras = new Bundle();
        extras.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        AccountManager am = AccountManager.get(getContext());
        Account[] acc = am.getAccountsByType("com.google");
        Account account = null;
        if (acc.length > 0) {
            account = acc[0];
            ContentResolver.requestSync(account, "com.android.calendar", extras);
        }
    }

    private <T> void sendNotifications(Iterable<T> oldList, Iterable<T> newList, String title, StringInterface<T> titleInterface, StringInterface<T> idInterface, String moduleId, int modulePart, @StringRes int updateRes, @StringRes int addRes, @StringRes int removeRes) {
        sendNotifications(oldList, newList, title, titleInterface, idInterface, moduleId, modulePart, updateRes, addRes, removeRes, null, null, null);
    }

    private <T> void sendNotifications(Iterable<T> oldList, Iterable<T> newList, String title, StringInterface<T> titleInterface, StringInterface<T> idInterface, String moduleId, int modulePart, @StringRes int updateRes, @StringRes int addRes, @StringRes int removeRes, ArrayList<T> changesAdd, ArrayList<Pair<T, T>> changesUpd, ArrayList<T> changesDel) {
            if (oldList == null || newList == null) {
            return;
        }
        ArrayList<T> obsoletes = new ArrayList<>();
        for (T old: oldList) {
            obsoletes.add(old);
        }
        String targetData = moduleId+"."+ModulePart.getPageByPart(modulePart);
        for (T newEntry : newList) {
            boolean found = false;
            for (T oldEntry: oldList) {
                if (idInterface.get(newEntry).equals(idInterface.get(oldEntry))) {
                    found = true;
                    if (newEntry.hashCode() != oldEntry.hashCode()) {
                        CustomNotificationManager.sendNotification(getContext(), getContext().getString(updateRes, title), titleInterface.get(newEntry), FRAGMENT_MODULES_DETAILS, targetData);
                        if (changesUpd != null) {
                            changesUpd.add(new Pair<>(oldEntry, newEntry));
                        }
                    }
                    obsoletes.remove(oldEntry);
                    break;
                }
            }
            if (!found) {
                CustomNotificationManager.sendNotification(getContext(), getContext().getString(addRes, title), titleInterface.get(newEntry), FRAGMENT_MODULES_DETAILS, targetData);
                if (changesAdd != null) {
                    changesAdd.add(newEntry);
                }
            }
        }
        for (T oldEntry : obsoletes) {
            CustomNotificationManager.sendNotification(getContext(), getContext().getString(removeRes, title), titleInterface.get(oldEntry), FRAGMENT_MODULES_DETAILS, targetData);
            if (changesDel != null) {
                changesDel.add(oldEntry);
            }
        }
    }

    @FunctionalInterface
    interface StringInterface<T> {
        String get(T element);
    }
}
