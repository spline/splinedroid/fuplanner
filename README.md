# FUPlanner

App to interface with the [Whiteboard](https://mycampus.imp.fu-berlin.de/) and
(technically) the [Blackboard](https://lms.fu-berlin.de/). 

## installation

Soon:tm: via F-Droid, the old version is currently available in the common
proprietary stores, or from source. 

## Building

If you want to build you can use a common Java IDE like IntelliJ or from CLI
with `gradle build`.

brought to you by Spline:tm:
